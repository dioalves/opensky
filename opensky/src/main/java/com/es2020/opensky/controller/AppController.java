package com.es2020.opensky.controller;

import com.es2020.opensky.AirplaneService;
import com.es2020.opensky.model.Airplane;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class AppController {

    private static final Logger LOG = LoggerFactory.getLogger(AppController.class);
    
    @Autowired
    private AirplaneService airplaneService;

    @GetMapping("/list")
    public String list(@RequestParam(required = false) String country, Model model) throws JsonProcessingException, IOException {

        System.out.println(country);

        List<Airplane> airplanes = (List<Airplane>)airplaneService.findAll();
        HashSet<String> countries = new HashSet<>();

        /*countries.add(origin_country);

        if (country != null && !country.isEmpty()) {
            if (origin_country.equalsIgnoreCase(country)) {
                //airplanes.add(new Airplane(icao24, velocity, origin_country));
            }
        } else {
            //airplanes.add(new Airplane(icao24, velocity, origin_country));
        }*/
        
        //airplanes = airplaneService.findAll();
        
        System.out.println(airplanes);
        
        model.addAttribute("airplanes", airplanes);
        model.addAttribute("countries", countries);
        return "list";
    }

}
