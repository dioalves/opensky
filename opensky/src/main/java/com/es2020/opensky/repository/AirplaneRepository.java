package com.es2020.opensky.repository;

import com.es2020.opensky.model.Airplane;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AirplaneRepository extends JpaRepository<Airplane, Long>{
    
    
    
}
