package com.es2020.opensky.kafka;

import java.util.Properties;
import org.apache.kafka.clients.consumer.KafkaConsumer;


public class Consumer {
    
    KafkaConsumer kafkaConsumer;
    
    public Consumer() {
        
        Properties properties = new Properties();
        properties.put("bootstrap.servers", "localhost:9092");
        properties.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        properties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        properties.put("group.id", "opensky-group");
        
        this.kafkaConsumer = new KafkaConsumer(properties);
    }
    
}
