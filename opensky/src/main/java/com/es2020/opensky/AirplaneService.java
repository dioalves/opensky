package com.es2020.opensky;

import java.util.*;

import com.es2020.opensky.model.Airplane;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.persistence.Query;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class AirplaneService {

    @PersistenceContext
    private EntityManager entityManager;

    public long insert(Airplane airplane) {
        entityManager.persist(airplane);
        return airplane.getId();
    }

    public Airplane find(long id) {
        return entityManager.find(Airplane.class, id);
    }

    public List< Airplane> findAll() {
        Query query = entityManager.createNamedQuery(
                "query_find_all_airplanes", Airplane.class);

        return query.getResultList();
    }
}
