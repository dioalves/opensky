package com.es2020.opensky.model;

import javax.persistence.*;


@Entity
@NamedQuery(query = "select u from Airplane u", name = "query_find_all_airplanes")
public class Airplane {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String icao24;
    private double velocity;
    private String origin_country;

    
    public Long getId() {
        return id;
    }
    
    public String getOrigin_country() {
        return origin_country;
    }

    public double getVelocity() {
        return velocity;
    }

    public String getIcao24() {
        return icao24;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setIcao24(String icao24) {
        this.icao24 = icao24;
    }

    public void setOrigin_country(String origin_country) {
        this.origin_country = origin_country;
    }

    public void setVelocity(double velocity) {
        this.velocity = velocity;
    }
    
    @Override
    public String toString() {
        return this.icao24 + " " + this.origin_country;   
    }
}
