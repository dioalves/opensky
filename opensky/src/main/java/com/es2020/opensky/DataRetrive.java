package com.es2020.opensky;

import com.es2020.opensky.kafka.KafkaSender;
import com.es2020.opensky.model.Airplane;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.HashMap;
import java.util.Map;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class DataRetrive {

    @Autowired
    private AirplaneService airplaneService;

    @Autowired
    KafkaSender kafkaSender;

    @Scheduled(fixedDelay = 10000)
    public void cronJob() throws JsonProcessingException {

        RestTemplate restTemplate = new RestTemplate();
        String openSkyUrl = "https://opensky-network.org/api/states/all";
        ResponseEntity<String> response = restTemplate.getForEntity(openSkyUrl, String.class);
        //OpenskyProducer producer = new OpenskyProducer(template);

        if (response.getStatusCode() == HttpStatus.OK) {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode responseTree = mapper.readTree(response.getBody());

            if (responseTree.get("states").isArray()) {

                for (JsonNode state : responseTree.get("states")) {

                    String icao24 = state.get(0).asText();
                    double velocity = state.get(9).asDouble();
                    String origin_country = state.get(2).asText();

                    Airplane airplane = new Airplane();
                    airplane.setIcao24(icao24);
                    airplane.setOrigin_country(origin_country);
                    airplane.setVelocity(velocity);

                    kafkaSender.send(icao24);

                    System.out.println(airplane.toString());
                    airplaneService.insert(airplane);
                }
            }
        }
    }
}
