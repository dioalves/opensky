package com.es2020.openskyconsumer.controller;

import java.util.Properties;
import org.apache.kafka.clients.consumer.KafkaConsumer;

public class AppController {

    KafkaConsumer kafkaConsumer;

    public AppController() {

        Properties properties = new Properties();
        properties.put("bootstrap.servers", "localhost:9092");
        properties.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        properties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        properties.put("group.id", "opensky");

        this.kafkaConsumer = new KafkaConsumer(properties);
    }
    
    
    

}
