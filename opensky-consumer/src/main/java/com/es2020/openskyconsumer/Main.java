package com.es2020.openskyconsumer;

import java.io.IOException;
import java.util.logging.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties.Producer;
import org.springframework.stereotype.Service;
import org.springframework.kafka.annotation.KafkaListener;

@SpringBootApplication
public class Main {

    @Service
    public class Consumer {

        @KafkaListener(topics = "opensky", groupId = "group_id")
        public void consume(String message) throws IOException {
            System.out.println(String.format("#### -> Consumed message -> %s", message));
        }
    }

    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }

}
